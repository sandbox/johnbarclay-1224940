<?php


function field_api_schema_datetime(&$field) {
	$field['field_type'] = 'datetime';
	$field['type'] = 'datetime';
	$field['repeat'] = 0;
	$field['todate'] = 'optional';
	$field['widget_type'] = 'date_select';
	$field['tz_handling'] = 'site';
	$field['granularity'] = array('year' => 'year', 'month' => 'month', 'day' => 'day', 'hour' =>'hour', 'minute' => 'minute');
	$field['year_range'] = '-3:+3';
	$field['input_format'] = date_default_format($field['widget_type']);
	$field['text_parts'] = array();
	$field['increment'] = 1;
	$field['default_value'] = 'blank';
	$field['default_value2'] = 'blank';
	$field['default_format'] = 'medium';
}


function field_api_maps_datetime(&$mapping) {
	$mapping['field_type'][]     = "['field_type']";
	$mapping['type'][]           = "['field_type']";
	$mapping['repeat'][]         = "['repeat']";
	$mapping['todate'][]         = "['todate']";
	$mapping['widget_type'][]    = "['widget_type']";
	$mapping['tz_handling'][]    = "['tz_handling']";
	$mapping['granularity'][]    = "['granularity']";
	$mapping['year_range'][]     = "['year_range']";
	$mapping['input_format'][]   = "['input_format']";
	$mapping['increment'][]      = "['increment']";
	$mapping['default_value'][]  = "['default_value']";
	$mapping['default_value2'][] = "['default_value2']";
	$mapping['default_format'][] = "['default_format']";
}


   /**
   * Create a date field from an array of settings values.
   * All values have defaults, only need to specify values that need to be different.
   */
  function field_api_create_date($values = array(), $create_field = TRUE, $create_instance = TRUE) {
    extract($values);

    $field_name = !empty($field_name) ? $field_name : 'field_test';
    $entity_type = !empty($entity_type) ? $entity_type : 'node';
    $bundle = !empty($bundle) ? $bundle : 'story';
    $label = !empty($label) ? $label : 'Test';
    $field_type = !empty($field_type) ? $field_type : 'datetime';
    $repeat = !empty($repeat) ? $repeat : 0;
    $todate = !empty($todate) ? $todate : 'optional';
    $widget_type = !empty($widget_type) ? $widget_type : 'date_select';
    $tz_handling = !empty($tz_handing) ? $tz_handling : 'site';
    $granularity = !empty($granularity) ? $granularity : array('year', 'month', 'day', 'hour', 'minute');
    $year_range = !empty($year_range) ? $year_range : '2010:+1';
    $input_format = !empty($input_format) ? $input_format : date_default_format($widget_type);
    $text_parts = !empty($text_parts) ? $text_parts : array();
    $increment = !empty($increment) ? $increment : 15;
    $default_value = !empty($default_value) ? $default_value : 'now';
    $default_value2 = !empty($default_value2) ? $default_value2 : 'blank';
    $default_format = !empty($default_format) ? $default_format : 'long';

    $field = array(
      'field_name' => $field_name,
      'type' => $field_type,
      'cardinality' => !empty($repeat) ? FIELD_CARDINALITY_UNLIMITED : 1,
      'settings' => array(
        'granularity' => $granularity,
        'tz_handling' => $tz_handling,
        'timezone_db' => date_get_timezone_db($tz_handling),
        'repeat' => $repeat,
        'todate' => $todate,
        ),
    );
    $instance = array(
      'entity_type' => $entity_type,
      'field_name' => $field_name,
      'label' => $label,
      'bundle' => $bundle,
      // Move the date right below the title.
      'weight' => -4,
      'widget' => array(
        'type' => $widget_type,
        // Increment for minutes and seconds, can be 1, 5, 10, 15, or 30.
        'settings' => array(
          'increment' => $increment,
          // The number of years to go back and forward in drop-down year selectors.
          'year_range' => $year_range,

          'input_format' => $input_format,
          'text_parts' => $text_parts,
          'label_position' => 'above',
          'repeat_collapsed' => 0,
        ),
        'weight' => -4,
      ),
      'settings' => array(
        'default_value' => $default_value,
        'default_value2' => $default_value2,
        'default_format' => $default_format,
      )
    );

    $instance['display'] = array(
      'default' => array(
        'label' => 'above',
        'type' => 'date_default',
        'settings' => array(
          'format_type' => $default_format,
          'show_repeat_rule' => 'show',
          'multiple_number' => '',
          'multiple_from' => '',
          'multiple_to' => '',
          'fromto' => 'both',
        ),
        'module' => 'date',
        'weight' => 0 ,
      ),
      'teaser' => array(
        'label' => 'above',
        'type' => 'date_default',
        'weight' => 0,
        'settings' => array(
          'format_type' => $default_format,
          'show_repeat_rule' => 'show',
          'multiple_number' => '',
          'multiple_from' => '',
          'multiple_to' => '',
          'fromto' => 'both',
        ),
        'module' => 'date',
      ),
    );
    if ($create_field) {
			$field = field_create_field($field);
		}
		if ($create_instance) {
			$instance = field_create_instance($instance);
		}

    //_field_info_collate_fields(TRUE);
    field_info_cache_clear(TRUE);
    field_cache_clear(TRUE);

  }
