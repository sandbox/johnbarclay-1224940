<?php

/*
Version: 1.0
Plugin Name: University of Illinois Web Tools Calendar Plugin
Plugin URI: http://webmasters.illinois.edu/university+of+illinois+web+tools+calendar+plugin
Description: A widget to display a U of I Web Tools calendar on your WordPress site.
Author URI: http://dashfien.com
Author: David Dashifen Kees
*/

if(!class_exists("WP_Http")) include_once(ABSPATH . WPINC. "/class-http.php");
register_activation_hook(__FILE__, "web_tool_cal::activation");
add_action("widgets_init", "web_tool_cal::load_widget");


class web_tool_cal extends WP_Widget {
	const default_widget_title = "Upcoming Events";
	const default_max_events = 10;
	const default_end_date = 10;
	const version = "0.0.1";

	protected $web_tool_cal_dir;			// can't be a constant because we want to use the pathinfo() function to get it.
		
	/* STATIC METHODS 
	 * The static methods of this class faciliate interaction between the plugin and WordPress.  Many are referenced in
	 * the hooks, filters, actions, shortcodes, etc. that appear prior to the class definition above.  Because we use
	 * static methods to do this we won't collide with other function names in other plugins (or in WordPress) but it 
	 * does mean that this plugin requires PHP 5.
	 */

	public static function activation() {
		$version = get_option("web_tool_cal_version", 0);		// get the current version or default to zero.
		if($version < self::version) {
			// if the version in the WP database is less than that of this plugin, then it indicates that the plugin
			// was updated.  within this block we can handle any changes that need to be made as a result of that 
			// update.  
	
			// now that we're done with those updates, we'll want to ensure we don't re-run this block again until
			// the next time the plugin is updated.  thus, we update the WP database with the new version number of
			// this plugin as follows:
				
			update_option("web_tool_cal_version", self::version);
		}
	}
	
	public static function load_widget() { register_widget("web_tool_cal"); }
		
	/* WIDGET FUNCTIONALITY
	 * If you want to use this class as a widget, thet's totally cool with us.  The following methods will ensure that 
	 * you can show the calendar within your theme in a sidebar of your choosing.  The constructor is a little more complex
	 * than the average widget, but we do our best to explain it below.  Then, the usual form(), update(), and widget() 
	 * classes are there so that we can change it's settings, save those settings, and print it respectively.
	 */
	
	public function __construct() {
		// odd that the constructor is way down here, no?  well, it's because the constructor is needed for widget
		// functionality.  but, we also use some of the other methods when displaying the calendar in a page or 
		// post via the shortcodes provide above.  when we instantiate this object within this code, we pass in a
		// reset flag which tells us not to do all the widget stuff.  but, because WP will instantiate this object
		// without passing it any arguments, we can't rely on a name and instead turn toward PHP's ability to analzye
		// functional arguments.
		
		$pathinfo = pathinfo(__FILE__);
		$this->web_tool_cal_dir = $pathinfo["dirname"];
		
		if(sizeof(func_get_args()) == 0) {
			// if we've note received arguments, then this object is getting instantiated as a part of the WP widget
			// framework.  therefore, we'll handle all the widget stuff here.
			
			$widget_metadata = array(
				"classname"   => "Web Tools Calendar", 
				"description" => "A widget to show a U of I Web Tools Calendar."
			);
			
			$widget_structure = array("id_base" => "web-tool-cal-widget");
			
			// the WP_Widget class wants to know (a) the name of our widget, (b) the name of the class which handles it,
			// (c) the metadata about the widget, and (d) the structure the widget needs when displayed.  to keep things
			// simple, ew use the same name (i.e., item (a)) as the ID that our structure will use on the widget's HTML
			// element.
			
			parent::__construct("web-tool-cal-widget", "web_tool_cal", $widget_metadata, $widget_structure );
		}
	}
	
	public function form($instance) {
		// the form function displays the input fields that allow a user to change the widget settings in the 
		// Appearance -> Widgets dashboard area.  for this widget we'll need the web tools calendar ID, the date 
		// range during which to request events, and the maximum number of events to show.
		
		$defaults = array(
			"web_tool_cal_calendar_id"  => "",
			"web_tool_cal_widget_title" => self::default_widget_title,
			"web_tool_cal_end_date"     => self::default_end_date,						// this number is in days
			"web_tool_cal_max_events"   => self::default_max_events
		);
		
		// using the wp_parse_args, we will replace missing information in $instance with the information in $defaults.
		// this sets our defaults on the first use of the widget and then leaves all changes alone thereafter.

		$instance = wp_parse_args((array) $instance, $defaults); ?>	
		
		<p>
			<label for="<?php echo $this->get_field_id("web_tool_cal_widget_title"); ?>">Widget Title:</label>
			<input id="<?php echo $this->get_field_id("web_tool_cal_widget_title"); ?>" name="<?php echo $this->get_field_name("web_tool_cal_widget_title"); ?>" value="<?php echo $instance["web_tool_cal_widget_title"]; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("web_tool_cal_calendar_id"); ?>">Web Tools Calendar ID:</label>
			<input id="<?php echo $this->get_field_id("web_tool_cal_calendar_id"); ?>" name="<?php echo $this->get_field_name("web_tool_cal_calendar_id"); ?>" value="<?php echo $instance["web_tool_cal_calendar_id"]; ?>">
		</p>
		
		<p>This widget will show all events on your calendar from the current date to another one in the future.  To specify 
		that date, enter the maximum number of days to add to the current date in the first field below.  Then, you can limit 
		the total number of events displayed using the next one.</p>
		
		<p>
			<label for="<?php echo $this->get_field_id("web_tool_cal_end_date"); ?>">Date Range to Display:</label>
			<input id="<?php echo $this->get_field_id("web_tool_cal_end_date"); ?>" name="<?php echo $this->get_field_name("web_tool_cal_end_date"); ?>" value="<?php echo $instance["web_tool_cal_end_date"]; ?>"> days
		</p>
		<p>
			<label for="<?php echo $this->get_field_id("web_tool_cal_max_events"); ?>">Maximum Number of Events:</label>
			<input id="<?php echo $this->get_field_id("web_tool_cal_max_events"); ?>" name="<?php echo $this->get_field_name("web_tool_cal_max_events"); ?>" value="<?php echo $instance["web_tool_cal_max_events"]; ?>">
		</p>
	<? }
	
	public function update($new_instance, $old_instance) {
		// a newly created instance of the widget is passed here first and then the old instance.  we make a "copy" of 
		// $old_instance because that's what the WP docs tell me to do; I don't really understand it, to be honest but
		// everyone does it so there might be a reason.
	
		$instance = $old_instance;
		$instance["web_tool_cal_widget_title"] = strip_tags($new_instance["web_tool_cal_widget_title"]);
		$instance["web_tool_cal_calendar_id"]  = $new_instance["web_tool_cal_calendar_id"];
		$instance["web_tool_cal_end_date"]     = $new_instance["web_tool_cal_end_date"];
		$instance["web_tool_cal_max_events"]   = $new_instance["web_tool_cal_max_events"];
		return $instance;
	}
	
	public function widget($args, $instance) {
		extract($instance);		// gives us local versions for *_widget_title, *_calendar_id, *_end_date, *_max_events
		extract($args);			// before_widget, before_title, after_title, after_widget as defined by WordPress settings
		
		// so that our widegt will still work with any filters that someone might be using, we'll ensure that the 
		// widget_title filter is applied to our widget's title on the following line.  then we have to actually get 
		// the events to display and do so.
		
		$filtered_title = apply_filters("widget_title", $web_tool_cal_widget_title);		
		$events = $this->get_events($web_tool_cal_calendar_id, $web_tool_cal_end_date, $web_tool_cal_max_events);
		if(sizeof($events) > 0) {
			// most of the following variables printed to the screen come out of $args when we extracted it above.
			// these are all subject to filtering based on a person's template so we don't really mess with them
			// here.  we'll use the following two sprintf() formats to do things simple-like in the loop below.
			
			echo "$before_widget $before_title $filtered_title $after_title <ul class='web-tool-cal-widget'>";
			foreach($events as $event) {
				extract($event);
				
				// since the $link is optional, we'll edit the $title to include the <a> tag if we need to.
				// then for the date, time, and room information, we'll add some handy prepositions if we need
				// them to make our display feel a little bit more like a sentence.
				
				$title = !empty($link)
					? "<a href=\"$link\"><span class=\"web-tool-cal-title\">$title</span></a>"				
					: "<span class='web-tool-cal-title'>$title</span>";
					
				$item = "<li>$title ";
				if(!empty($date)) $item .= "on <span class='web-tool-cal-date'>$date</span> ";
				if(!empty($time)) $item .= "at <span class='web-tool-cal-time'>$time</span> ";
				if(!empty($room)) $item .= "in <span class='web-tool-cal-room'>$room</span> ";
				$item .= "</li>";
				echo $item;
			}
			
			echo "</ul> $after_widget <!-- $this->web_tool_cal_dir -->";
		} else echo "No Events.";
	}
	
	/* EVENT FUNCTIONS 
	 * These functions are used both by the widget functionality and the shortcode printing (for pages and posts) 
	 * to actually gather events from a web tools calendar and present them on screen.  They facilitate the fetching
	 * and local storing of XML data from web tools and the creation of an array of event objects that will provide
	 * us a way to present on-screen information for events.  They are all proteted methods.
	 */
	
	protected function get_events($web_tool_cal_calendar_id, $web_tool_cal_end_date, $web_tool_cal_max_events) {
		// the process of getting our events is two fold:  (1) get the XML and then (2) parse the XML.  this method
		// handles the parsing, but shoves off the gathering of the XML to the following ones.
	
		$event_xml = $this->fetch_event_xml($web_tool_cal_calendar_id, $web_tool_cal_end_date);
		$event_xml = new SimpleXMLElement($event_xml);
		
		// at this point, we've created a SimplXMLElement object that we can use to perform our parse.  the XML 
		// we have is a series of publicEventWS objects which contain the data we need.  these we turn into a 
		// series of PHP arrays that are returned to the calling scope.
		
		$results = array();
		foreach($event_xml->publicEventWS as $event) {
			$temp = array();
			$temp["title"] = (string) $event->titleLong;			// casted as strings to avoid adding SimpleXMLElement
			$temp["link"]  = (string) $event->titleLink;			// instances into our array.  cuts down on memory usage
			$temp["date"]  = (string) $event->startDate;			// and they'd get converted to strings when displayed
			$temp["time"]  = (string) $event->startTime;			// anyway.  
			$temp["room"]  = (string) $event->locationText;
			$results[] = $temp;
		}
		
		return $results;	
	}
	
	protected function fetch_event_xml($web_tool_cal_calendar_id, $web_tool_cal_end_date) {
		// our end date isn't actually a date.  it's the number of days after the current date for which we want to
		// receive XML information from the server.  so first we're going to create our start and ending dates in 
		// MM/DD/YYYY format as expected by the web tools system.  if $web_tool_cal_end_date is invalid, we go with
		// the default set above.
		
		$now = time();	
		$start_date = date("n/j/Y", $now);
		$xml_filename = $this->web_tool_cal_dir . "/calendar" . $web_tool_cal_calendar_id . ".xml";
		
		
		// invalid end dates include non-numeric entries as well as entries less than one.  then we also floor() the
		// number to ensure that we don't have a floating point number that's greater than or equal to one.
		
		if(!is_numeric($web_tool_cal_end_date) || $web_tool_cal_end_date < 1) $web_tool_cal_end_date = self::default_end_date;
		$end_date = date("m/j/Y", strtotime("+" . floor($web_tool_cal_end_date) . " days", $now));
	
		// to try and limit the number of times that we have to fetch XML from the web tools servers, we see if the XML we 
		// currently have is older than 15 minutes.  if it is younger than that, we just use it.  otherwise, we get new XML
		// from the web tools below.
		
		if(is_file($xml_filename) && ((time() - filemtime($xml_filename)) < 900)) return file_get_contents($xml_filename);
		
		// if we didn't return on the previous line, then we need to actually get XML from the web tools server.  we've
		// already included the code for the WordPress HTTP class, which we'll use to simplify the gathering of that information.  
		// first, we'll build the URL that we're going to need to request, then we'll get the XML and // manage it.  more on the 
		// WP_Http class: http://planetozh.com/blog/2009/08/how-to-make-http-requests-with-wordpress/
		
		$http = new WP_Http();
		$url = sprintf("http://illinois.edu/calendar/eventXML/%s.xml?startDate=%s&endDate=%s",
			$web_tool_cal_calendar_id, $start_date, $end_date);
			
		$results = $http->request($url);						// this method will get our XML from the web tools server
		
		if(!empty($results["body"])) {
			// if we received information from the web tools server, then we want to ensure that it's valid XML.  to do this
			// we're simply going to try and open it as a SimpleXMLElement which throws an exception if the data isn't XML.
						
			try { $xml = new SimpleXMLElement($results["body"]); }
			catch (Exception $e) { return file_get_contents($xml_filename); }
				
			// if we were successful in opening the $results['body'] as an XML object, then we want to write that information
			// to the filesystem to ensure that we have a cache for the future if we need it.  then we can return that information
			// to the calling scope.
				
			@file_put_contents($xml_filename, $results["body"]);	// @ op to avoid problems for those with incorrect folder perms
			return $results["body"];
		}
				
		// if we're all the way down here, then we got empty $results above.  we know this because the catch-block has a return
		// statement in it as does the if-block above.  thus, if we had either an error or valid information, then we have returned.
		// thus, here, we have no information at all.  to handle this, we'll just return the old information.
		
		return file_get_contents($xml_filename);	
	}
} ?>