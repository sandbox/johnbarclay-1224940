<?php

/*
Version: 1.0
Plugin Name: University of Illinois Web Tools Calendar Plugin
Plugin URI: http://webmasters.illinois.edu/university+of+illinois+web+tools+calendar+plugin
Description: A widget to display a U of I Web Tools calendar on your WordPress site.
Author URI: http://dashfien.com
Original Author: David Dashifen Kees wrote the guts of this, but its been reworked
*/




function wt_cal_url($calendar_id = 7, $start_date = '1/1/2001', $end_date = '1/1/2999') {
	// eg http://illinois.edu/calendar/eventXML/7.xml?startData=1/1/2001&endDate=1/1/2999
	return WT_CAL_BASE_URL . $calendar_id . '.xml?startDate='. $start_date . '&endDate='. $end_date;
}

function wt_cal_get_events($calendar_id, $start_date = NULL, $end_date = '1/1/2999', $max_events = WT_CAL_DEFAULT_MAX_EVENTS) {
//	$max_events = 1;
	  if (!$start_date) {
			$start_date = date('m/d/Y', time() - 3600 * 12); // go back a day by default
		}
		$url = wt_cal_url($calendar_id, $start_date, $end_date);
		$event_xml  = file_get_contents($url);
		$event_xml = new SimpleXMLElement($event_xml);

		$results = array();
		$fields = wt_cal_schema_to_short_schema();

		$count = 0;
		foreach($event_xml->publicEventWS as $event) {
			$event_id = (string)$event->eventId;
			$results[$event_id] = array();
			foreach($fields as $field_name => $attr) {
				$results[$event_id][$field_name] = (string)$event->{$field_name};
			}

			// check for html and text field override fields.
			// e.g. descriptionHtml || descriptionText merged into description field
			foreach($fields as $field_name => $attr) {
				if ($attr['html']) {
					$text_field_name = str_replace('Html', 'Text', $field_name);
					$override_name = str_replace('Html', '', $field_name);
					if (isset($fields[$text_field_name])) { // only create
						$results[$event_id][$override_name] = ($results[$event_id][$field_name]) ? $results[$event_id][$field_name] : $results[$event_id][$text_field_name];
					}
				}
			}

			// generate iso date from webtools date fields
			$time_type = $results[$event_id]['timeType'];
			$debug = array('startDate' => $results[$event_id]['startDate'], 'startTime' => $results[$event_id]['startTime'], 'time_type' => $time_type);
			$startDateTime = wt_cal_webtools_to_unixtime($results[$event_id]['startDate'], $results[$event_id]['startTime'], ($time_type == 'START_TIME_ONLY' || $time_type == 'START_AND_END_TIME'));
			if ($time_type == 'START_AND_END_TIME') {
				$endDateTime = wt_cal_webtools_to_unixtime($results[$event_id]['endDate'], $results[$event_id]['endTime'], ($time_type == 'START_AND_END_TIME'));
			}
			else {
				$endDateTime = $startDateTime;
			}
			$debug['startDateTime unix'] = $startDateTime;
			$debug['endDateTime unix'] = $endDateTime;
			$results[$event_id]['startDateTime'] =  $startDateTime; // date(DATE_ISO8601, $startDateTime);
			$results[$event_id]['endDateTime'] =  $endDateTime; //date(DATE_ISO8601, $endDateTime);

		//	watchdog('status', print_r($debug, TRUE));

			$count++;
			if ($count == $max_events) {
				break;
			}

		}
		return $results;
	}


/**
 * convert webtools datetime fields to unixtime
 */

function wt_cal_webtools_to_unixtime($date, $time = NULL, $include_time = FALSE) {
 // watchdog('status', "date $date. time $time, include_time = $include_time");
	list($month, $day, $year) = explode('/', $date);
	if ($time && $include_time) {
		list($time, $am_pm) = explode(' ', $time);
		$time_parts = explode(':', $time);
		$hr = (isset($time_parts[0])) ?  $time_parts[0] : 0;
		$min = (isset($time_parts[1])) ?  $time_parts[1] : 0;
		$hr_offset = ($am_pm == 'pm' && $hr != '12') ? 12 : 0;
    $hr = $hr + $hr_offset;
	}
	else {
		$hr = 0;
		$min = 0;
	}
  //watchdog('status', print_r(array($hr, $min, 0, $month, $day, $year), TRUE));
	//watchdog('status', mktime($hr, $min, 0, $month, $day, $year));
	return mktime($hr, $min, 0, $month, $day, $year);
}

/** this function is not used, but may be useful in the future
*/
function wt_add_date_module_date(&$event) {

	include_once(drupal_get_path('module', 'date_api') .'/date_api_ical.inc');

  $item['ical_date'] = new FeedsDateTimeElement($start, $end, $tz);


    if (empty($feed_element['DTSTART']['datetime'])) {
      return;
    }
    include_once(drupal_get_path('module', 'date_api') .'/date_api_ical.inc');

    $timezone = $feed_element['DTSTART']['tz'];
    if (!empty($timezone)) {
      $timezone = new DateTimeZone($timezone);
    }
    $this->start = new FeedsDateTime($feed_element['DTSTART']['datetime'], $timezone);
    if (!empty($feed_element['DTEND']) && !empty($feed_element['DTEND']['datetime'])) {
      $this->end = new FeedsDateTime($feed_element['DTEND']['datetime'], $timezone);
    }
    if ($feed_element['DTSTART']['all_day']) {
      // All day event; remove time granularity, set to = from
      $this->start->setTime(0, 0, 0);
      $this->start->removeGranularity('hour');
      $this->start->removeGranularity('minute');
      $this->start->removeGranularity('second');
      $this->end = clone $this->start;
    }

}
