<?php

/**
 * @file
 * functions specific to webtools event content type
 */

/**
 * function to create a node type for events
 */
function wt_cal_create_event_ct($bundle = WT_CAL_CT_BUNDLE, $name = WT_CAL_CT_NAME) {

  // 1. create content type

  $form = array();
  $form_state = array(
    'values' => array(
      'name' => $name,
      'type' => $bundle,
      'description' => 'Content type for Webtools Calendar Events',
      'title_label' => 'Title',
      'node_submitted' => 1,
      'base' => 'node_content',
      'old_type' => '',
      'orig_type' => '',
      'base' => '',
      'custom' => 1,
      'modified' => 0,
      'locked' => 0,
			'help' => '',
      'submit' => t('Save content type'),
      'save_continue' => t('Save and add fields'),
      'form_id' => 'node_type_form',
      'op' => t('Save content type'),
    ),
  );

  require_once(drupal_get_path('module','node') . '/content_types.inc');
  node_type_form_validate($form, $form_state);
  if (form_get_errors()) {
    return;
  }
  node_type_form_submit($form, $form_state);

	$instance = array(
      'entity_type' => 'node',
      'bundle' => $bundle,
			'name' => $name,
    );
	$field_full_schema = wt_cal_ct_fields($bundle);

  field_api_create_fields($instance, $field_full_schema);

}

/**
 * generate all field definitions in "field_api" array
 * format
 */

function _wt_cal_ct_fields($bundle) {

	// 1. get fields mapped from webtools schema to default "field_api" schema
	$field_api_short_schema = wt_cal_schema_to_short_schema();


  // 2. add or remove fields or modify fields while fields are
	// still in "field_api" schema

	$defaults['calendarId']['weight'] = 10;
  $defaults['recurrance']['default_value'] = 0;

	foreach (array('description', 'location', 'speaker', 'sponsor') as $field_stem) {
		$defaults[$field_stem . 'Text']['type'] = 'text_long';
		$defaults[$field_stem . 'Html']['type'] = 'text_long';
		$defaults['$field_stem']['type'] = 'text_long';
	}

 // $schema = array(); // temp clear our for debugging.
  $field_api_short_schema['DrupalDateTimeField'] = array(
		'name' => 'EventDateTime',
		'type_ns' => 'xsd:drupalDateTime',
		'type' => 'datetime',
		'html' => 0,
		'feed_mapping_ignore' => TRUE,
	);

  // 3. do automated mapping of "field_api" schema to
	// various field and content type formats used in field_api_create_field function
	$fields_full_schema = array();
	foreach($field_api_short_schema as $wt_name => $attr) {
		$default = isset($defaults[$wt_name]) ? $defaults[$wt_name] : array();
		$fields_full_schema[$wt_name] = field_api_short_to_full_schema($attr, $bundle, $default, WT_CAL_CT_FIELD_PREFIX);
	}

  // 4. override any exceptions


  return $fields_full_schema;
}
