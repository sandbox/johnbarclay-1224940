<?php

/**
 * @file
 * Administrative page callbacks for the wt_cal module.
 */

/**
 * one big form for calendar administration.  keep it big untils its too big.
 * @return drupal form array
 */

function wt_cal_admin_form() {

  $settings = wt_cal_settings();
  $tokens = array();

  $form['intro'] = array(
    '#type' => 'item',
    '#markup' => t('<h1>Webtools Calendar Configuration</h1>', $tokens),
  );

  /**
   * feeds form
   */

  $form['calendars'] = array(
    '#type' => 'fieldset',
    '#title' => t('Calendar Feed Importers', $tokens),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['calendars']['new_cal_id'] = array(
    '#title' => t('Add a Calendar Importer'),
    '#type' => 'textfield',
    '#default_value' => NULL,
    '#description' => t('The numeric calendar id such as 7.  This will create a feed for that calendar to import the events into the Webtools Event content type.'),
    '#required' => FALSE,
    '#size' => 5,
  );

  $cal_options = array();
  foreach ($settings['feed_importers'] as $instance_id => $discard) {
    $instance = $settings['instances'][$instance_id];
    $calendar_id = $instance->wt_id;
    $cal_options[$calendar_id] = $calendar_id; // for use later in form
    $edit = l('edit feed', "admin/structure/feeds/edit/" .  $instance->importer->id);
    $form['calendars']['feed_enabled__' . $calendar_id] = array(
      '#type' => 'checkbox',
      '#title' => "Calendar $calendar_id (importer id: ". $instance->importer->id .", content type: ". $instance->importer->config['processor']['config']['content_type'] .") $edit",
      '#default_value' => !$instance->importer->disabled,
      '#description' => t('Uncheck to turn off this calendars feed.  This will not delete any content or feeds, it will simply disable the feed such that no new data will be retrieved.'),
    );
  }

  $form['calendars']['calendar_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 40,
    );

  /**
   * block form
   */

  $form['add_block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Calendar Block', $tokens),
    '#description' =>  !(count($cal_options)) ? t('Calendar Feed Importer must be added in form above before adding calendar blocks.') : '',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#disabled' => !(count($cal_options)),
  );

  $form['add_block']['new_block_title'] = array(
    '#title' => t('Title of Calendar Block'),
    '#type' => 'textfield',
    '#default_value' => NULL,
    '#required' => FALSE,
    '#size' => 60,
  );

  $form['add_block']['new_block_cal_id'] = array(
    '#title' => t('Calendar ID for Block'),
    '#type' => 'radios',
    '#options' => $cal_options,
    '#required' => FALSE,
    '#size' => 5,
  );

  $form['add_block']['new_block_machine_name'] = array(
    '#title' => t('Machine Name of Calendar Block.  Only lowercase letters and "_". e.g. 7_home'),
    '#type' => 'textfield',
    '#default_value' => NULL,
    '#required' => FALSE,
    '#description' => t('Additional configuration options available after block is created.'),
    '#size' => 15,
  );

  $form['add_block']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add Block'),
    '#weight' => 40,
    );

  $form['blocks'] = array(
    '#type' => 'fieldset',
    '#title' => t('Existing Calendar Blocks', $tokens),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // index of existing blocks to delete and edit
  $block_conf_links = array();
  if (module_exists('context')) {
    $table = array('header' => array('Cal Id', 'Machine Name', 'Title', 'Style', 'Action'), 'rows' => array());
  }
  else {
    $table = array('header' => array('Cal Id', 'Machine Name', 'Title', 'Style', 'Block Visibility', 'Block Pages', 'Action'), 'rows' => array());
  }
  $form_part = wt_cal_block_configure('dummy');
  foreach ($settings['blocks'] as $instance_id => $discard) {
    $display_type = ($display_type_id = @$instance->data['wt_cal_display_type']) ? $form_part['wt_cal']['wt_cal_display_type']['#options'][$display_type_id] : '';
    $instance = $settings['instances'][$instance_id];
    $row = array(
      $instance->wt_id,
      $instance->block->delta,
      $instance->block->title,
      $display_type,
    );
    if (!module_exists('context')) {
       $row[] =  $instance->block->visibility;
       $row[] =  $instance->block->pages;
    }
    $row[] =  l("Configure", "admin/structure/block/manage/wt_cal/" . $instance->block->delta . "/configure");
    $table['rows'][] = $row;
  }

  $form['blocks']['block_links'] = array(
    '#type' => 'item',
    '#markup' => theme('table', $table),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings'),
    '#weight' => 40,
    );

  return $form;

}


/**
 * validation of calendar admin form
 * @todo:  validate existing block deltas
 * @todo:  validate existing feed importer ids and calendar ids.
 */

function wt_cal_admin_form_validate($form, &$form_state) {
  $settings = wt_cal_settings();
  $values = $form_state['values'];
  if (isset($values['new_cal_id']) && isset($settings[$values['new_cal_id']])) {
    form_set_error('wt_cal', "Calendar " . $values['new_cal_id'] . " already has a feed.  It cannot be added again'");
  }
}


/**
 * submit handler function for ldap_authorization_admin_form
 */

function wt_cal_admin_form_submit($form, &$form_state) {

  $settings = wt_cal_settings();
  $values = $form_state['values'];
  $importers = feeds_importer_load_all();

  // create new importers if needed
  if (isset($values['new_cal_id']) && is_numeric($values['new_cal_id'])) {
    $proposed_cal_id = $form_state['values']['new_cal_id'];
    $importer = wt_cal_create_cal_importer($proposed_cal_id);
  }

  // enable and disable importers that are checked and unchecked
  $importer_enabled = array();
  foreach ($values as $field_id => $value) {
    $parts = explode('__',$field_id);
    if ($parts[0] == 'feed_enabled') {
      $calendar_id = $parts[1];
      $importer_id = 'wt_cal__feed_importer__' .  $calendar_id;
      $importer_enabled[$importer_id] = ($value == 0) ? 0 : 1;
    }
  }
  wt_api_xable_importers($importer_enabled);

  // add calendar blocks if present

  if ($values['new_block_cal_id'] && $values['new_block_machine_name']) {

    $delta = $values['new_block_machine_name'];
    $machine_name = join('__', array('wt_cal', 'block', $delta));
    wt_api_block_create('wt_cal', $delta, array());
    cache_clear_all(); // clear page and block cache

    $wt_record = new stdClass();
    $wt_record->machine_name = join('__', array('wt_cal', 'block', $values['new_block_machine_name']));
    $wt_record->module = 'wt_cal';
    $wt_record->wt_id = $values['new_block_cal_id'];
    $wt_record->drupal_structure = 'block';
    $wt_record->drupal_structure_id = 'wt_cal_' . $delta;
    wt_api_save('wt_cal', $machine_name, $wt_record);
  }

  return "submitted";

}

function wt_cal_create_cal_importer($calendar_id) {
  $prefix = 'wt_cal_';
	require_once(drupal_get_path('module','wt_cal') . '/wt_cal.ct.inc');
	$fields = wt_cal_ct_fields(WT_CAL_CT_BUNDLE);
	$mappings = array();
	$mappings[] = array(
		'source' => 'titleLong',
		'target' => 'title',
		'unique' => false,
	);

	$mappings[] = array(
		'source' => 'descriptionHtml',
		'target' => 'body',
		'unique' => 0,
	);

	$mappings[] = array(
		'source' => 'startDateTime',
		'target' => $prefix . 'event_date_time:start',
		'unique' => 0,
	);

	$mappings[] = array(
		'source' => 'endDateTime',
		'target' => $prefix . 'event_date_time:end',
		'unique' => 0,
	);
	$importer = wt_api_create_feed_importer('wt_cal', $calendar_id, 'wt_cal__feed_importer__'. $calendar_id, "Webtools Calendar $calendar_id Importer", array(), WT_CAL_GUID, WT_CAL_CT_BUNDLE, $fields, $mappings);
  return $importer;

}
