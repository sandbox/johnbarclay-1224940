<?php

require_once('wt_api.inc');

/**
 * add, edit, delete index for webtools page
 **/

function wt_api_pages_list($app_type = NULL) {

  $rows = array();
  if ($app_type) {
    global $wt_api_conf;
     $app_name = $wt_api_conf[$app_type]["name"];
     $result = db_query("SELECT wtid, path, app_type, instance_id, title FROM {wt_API} WHERE app_type = '%s'", array($app_type) );
     $heading = "$app_name Pages";
     $add_link = "<div class='add-link'>" . l("Add $app_name", WT_API_CONTENT_ROOT .'/calendar/add') . "</div>";
  } else {
     $result = db_query("SELECT wtid, path, app_type, instance_id, title FROM {wt_API}");
     $heading = "webtools pages";
  }

  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      $row->title,
      l($row->path, $row->path) ,
      $row->app_type,
      $row->instance_id,
      l(t('edit'), WT_API_CONTENT_ROOT. "/". $row->app_type . "/edit/". $row->wtid),
      l(t('delete'), WT_API_CONTENT_ROOT. "/". $row->app_type . "/delete/". $row->wtid)
    );
  }
  $header = array(
    t('Page Title'),
    t('URL'),
    t('Application Type'),
    t('Instance ID'),
    array('data' => t('Operations'), 'colspan' => 2),
  );



  return "<h2>$heading</h2>" . $add_link . theme('table', $header, $rows);
}

/**
 * form for editing or adding a single webtools page
 **/

function wt_api_pages_admin_form(&$form_state) {
  global $wt_api_conf;

  $app_type = arg(wt_API_APP_TYPE_ARG);
  $op = arg(wt_API_ACTION_ARG);
  $wtid = arg(wt_API_WT_ID_ARG); // $op, $wtid

  if ($op == "edit" && $wtid) {
    $page = db_fetch_array(db_query(
    "SELECT wtid, enabled, path, app_type, instance_id, title, default_args FROM {wt_api} WHERE wtid = %d", $wtid));
  } else {
    $page = array(
      'enabled' => 1,
      'path' => '',
      'app_type' => '',
      'instance_id' => '',
    );
  }

  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#description' => "Enable this page.",
    '#return_value' => TRUE,
    '#default_value' => $page['enabled'],
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#required' => TRUE,
    '#title' => t('Page Title'),
    '#description' => "",
    '#default_value' => $page['title'],
    );

  $form['path'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#required' => TRUE,
    '#title' => t('Path'),
    '#description' => "Path to page.  Should not start or end with slash.  e.g. campus-ethics-events/calendar",
    '#default_value' => $page['path'],
  );

  $form['app_type'] = array(
    '#type' => 'hidden',
    '#title' => t('Application Type'),
    '#description' => NULL,
    '#default_value' => arg(wt_API_APP_TYPE_ARG),
    '#required' => TRUE,
  );

  $form['instance_id'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#required' => FALSE,
    '#title' => t('Instance ID'),
    '#description' => "The id of the particular instance.  Such as 7 for calender 7.  Leave empty for
    apps such as moduleTest1 which don't need ids.",
    '#default_value' => $page['instance_id'],
    );

	$form['default_args'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Initial State'),
    '#description' => "When the path above is used with no arguments, what should be added
		  initially.",
    '#default_value' => ($page['default_args']) ? $page['default_args'] : wt_CAL_DEFAULT_ARGS,
    '#required' => FALSE,
  );


  if ($op == "edit" && $wtid) {

    $form['wtid'] = array(
        '#type' => 'hidden',
        '#value' => $wtid,
      );

    $form['buttons']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Update'),
      );

  } else {
     $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
    );
  }

  return $form;

}


/**
 * Validate hook for webtools page edit/add form.
 */
function wt_api_pages_admin_form_validate($form, &$form_state) {
  $path =  $form_state['values']['path'];
  // if path is being used
   if ($item = db_fetch_array(db_query("SELECT * FROM {menu_router} where path = '%s' ", $path ))) {
    if ($form_state['op'] == "Add" || $path != $form['path']['#default_value']) {
      form_set_error('', t('The path %path is already in use by the page %title.
        Please pick a different path; or change the path of %title first.',
        array('%path' => $path, '%title' => $item['title'])));
    }
   }
}

/**
 * Submit hook for the settings form.
 */
function wt_api_pages_admin_form_submit($form, &$form_state) {

  $values = $form_state['values'];

 if ($values['wtid']) {
  db_query("UPDATE {wt_API} SET
    title = '%s',
    path = '%s',
    app_type = '%s',
    instance_id = %d,
    enabled = %d,
    default_args = '%s'
    WHERE wtid = %d",
    array(
      $values['title'],
      $values['path'],
      $values['app_type'],
      $values['instance_id'],
      $values['enabled'],
      $values['default_args'],
      $values['wtid']
    ));
 } else {
    db_query("INSERT INTO {wt_API} SET
      title = '%s',
      path = '%s',
      app_type = '%s',
      instance_id = %d,
      enabled = %d,
      default_args = '%s'",
      array(
        $values['title'],
        $values['path'],
        $values['app_type'],
        $values['instance_id'],
        $values['enabled'],
        $values['default_args'],
      ));
 }

  menu_cache_clear_all(); // better to rebuild menu system
  drupal_set_message(t('The page has been saved.'));
  $form_state['redirect'] = wt_API_CONTENT_ROOT ."/". $values['app_type'];
}



/**
 * Implements the webtools page delete page.
 *
 * @param $form_state
 *   A form state array.
 * @param $wtid
 *   wtid, webtools page id
 *
 * @return
 *   The form structure.
 */

// wt_api_pages_admin_delete
function wt_api_pages_admin_delete(&$form_state, $wtid) {
  if ($row = db_fetch_object(db_query("SELECT title FROM {wt_api} WHERE wtid = %d", $wtid))) {
    $form['wtid'] = array(
      '#type' => 'hidden',
      '#value' => $wtid,
    );
    $form['title'] = array(
      '#type' => 'hidden',
      '#value' => $row->title,
    );
    return confirm_form(
      $form,
      t('Are you sure you want to delete the webtools page <em><strong>%title</strong></em>?
        This will only delete the record in within this Drupal site, not with the webtools toolbox.',
        array('%title' => $row->title)),
      wt_API_CONTENT_ROOT . '/list',
      t('<p>This action cannot be undone.</p>'),
      t('Delete'),
      t('Cancel')
    );
  }
  drupal_goto(wt_API_CONTENT_ROOT ."/". arg(wt_API_APP_TYPE_ARG) );
}

/**
 * Submit hook for the Webtools Page delete page.
 */
function wt_api_pages_admin_delete_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['confirm'] && $values['wtid']) {
    db_query("DELETE FROM {wt_api} WHERE wtid = %d", $values['wtid']);
    drupal_set_message(t('Webtools page %title has been deleted.', array('%title' => $values['title'])));
    watchdog('wt_api', 'Webtools pag %name has been deleted.', array('%title' => $values['title']));
  }
}

/**
 * admin form for webtools settings
 **/

function wt_api_settings() {

  $form['#title'] = "Configure Webtools Settings";


   $form['server'] = array(
    '#type' => 'fieldset',
    '#title' => t('Webtools Servlet Server'),
    '#weight' => -5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

 $form['server']['wt_api_qa'] = array(
      '#type' => 'checkbox',
      '#weight' => 5,
      '#title' => "Use QA Server?",
      '#return_value' => TRUE,
      '#default_value' => variable_get('wt_api_qa', FALSE),
    );

  $form['server']['wt_prod_baseurl'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#weight' => 7,
    '#required' => FALSE,
    '#title' => t('Webtools Production servlet url'),
    '#description' => "e.g. ". wt_PROD_BASEURL ." Do not add trailising slash.  This should be the
    full url to the application; but none of the arguments.",
    '#default_value' => variable_get('wt_prod_baseurl', wt_PROD_BASEURL),
    );

    $form['server']['wt_qa_baseurl'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#weight' => 7,
    '#required' => FALSE,
    '#title' => t('Webtools QA servlet url'),
    '#description' => "e.g. ". wt_QA_BASEURL ." Do not add trailising slash.  This should be the
    full url to the application; but none of the arguments.",
    '#default_value' => variable_get('wt_qa_baseurl', wt_QA_BASEURL),
    );


  $form['general']['wt_api_request_timeout'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#weight' => 7,
    '#required' => FALSE,
    '#title' => t('Request Timeout in Seconds'),
    '#description' => "How long to wait for content from webtools content before showing empty content",
    '#default_value' => variable_get('wt_api_request_timeout', wt_API_REQUEST_TIMEOUT),
    );


  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#weight' => -5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['general']['wt_api_key'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#weight' => 5,
    '#required' => FALSE,
    '#title' => t('Webtools Key'),
    '#description' => "Key from webtools.  Will be 10 charactes such as: 32jnfn24dsfwer",
    '#default_value' => variable_get('wt_api_key',""),
    );


   $form['general']['wt_api_request_timeout'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#weight' => 5,
    '#required' => FALSE,
    '#title' => t('Timeout in seconds'),
    '#description' => "How long to wait for webtools request before timing out.",
    '#default_value' => variable_get('wt_api_request_timeout',wt_API_REQUEST_TIMEOUT),
    );



  $form['general']['wt_api_nocontent_message'] = array(
    '#type' => 'textarea',
    '#title' => t("Content Empty Message"),
    '#default_value' => variable_get('wt_api_nocontent_message', wt_API_NOCONTENT_MESSAGE),
    '#cols' => 70,
    '#rows' => 6,
    '#weight' => 11,
    '#description' => t('When request for webtools content times out or returns no content, what text should be shown.'),
  );

  return system_settings_form($form);
}
