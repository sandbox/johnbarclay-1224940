<?php

global $wt_api_conf;
$wt_api_conf = array();

$wt_api_conf['calendar'] = array(
	"path" => 'calendar/moduleView',
  "argname" => "calId",
  "name" => "Calendar",
	"replace" => "Calendar"
);

// https://illinois.edu/blog/view/591
$wt_api_conf['blog'] = array(
	"path" => 'blog/moduleView',
  "argname" => "blogId",
  "name" => "Blog",
);

$wt_api_conf['form'] = array(
	"path" => 'form/moduleView',
  "argname" => "formId",
  "name" => "Form",
);

/**
 * Implements the webtools page delete page.
 *
 * @param $app_type
 *   calendar, blog, etc.
 * @param $instance_id
 *   id of particular instance of calendar, blog, etc.
 *
 * @return
 *   html to be inserted into content region of page.
 */

function wt_api_page($app_type, $instance_id, $drupal_path = NULL, $default_args = NULL) {
	global $wt_api_conf;

	if (strcasecmp(trim($_SERVER['REQUEST_URI'],'/'), trim($drupal_path,"/")) == 0 ) {
		$add = "?". $wt_api_conf[$app_type]['argname'] . "=". $instance_id . "&" . $default_args;
	}
	$debug_list = array();
	$debug_list[] = "App type: ". $app_type;
	$debug_list[] = "Instance ID: ". $instance_id;

  $url = wt_api_url($app_type, $instance_id, $drupal_path) . $add ;
	$debug_list[] = "URL: ". $url;

  $http_result = wt_api_get_web_page( $url );


	$debug_list[] = "http response code: ". $http_result['http_code'];


  if ($http_result['content'] != "") {
    $content .= wt_api_parse_urls($http_result['content'],$drupal_path);

  } else {
     $content .= variable_get('wt_api_nocontent_message',"");
  }

	if (variable_get('wt_api_qa', FALSE) == TRUE) {
		drupal_set_message("<p>This message box is a debugging message that only shows up when using the QA server.</p>" . theme_item_list($debug_list),($http_result['http_code'] == "200") ? 'status' : 'error');
	}
	return $content;
}

/**
 * given url of form:
 *   href="Calendar?ACTION=VIEW_EVENT&calId=7&skinId=1&DATE=3/31/2010&eventId=147983
 * convert to form:
 *   href="[calendar path]?ACTION=VIEW_EVENT&calId=7&skinId=1&DATE=3/31/2010&eventId=147983
 **/

function wt_api_parse_urls($html, $drupal_path) {

	$html = str_replace('href="Calendar?', 'href="/'. $drupal_path . "?" , $html );
	return $html;

}
/**
 * Get webtools content request url
 *
 * @param $app_type
 *   calendar, blog, etc.
 * @param $instance_id
 *   id of particular instance of calendar, blog, etc.
 * @param $base_url_and_path
 *  path to the displayed page in drupal, including domain such as http://www.education.illinois.edu/alumni/calendar
 * @return
 *   webtools content request url
 */
function wt_api_url($app_type, $instance_id, $drupal_base_path = NULL) {

  global $wt_api_conf;

  if (!$base_url_and_path) {
    global $base_url;
    $base_url_and_path = $base_url . $_SERVER['REQUEST_URI'];
  }

  $conf = $wt_api_conf[$app_type];
	if (variable_get('wt_api_qa', FALSE)) {
		$url = variable_get('wt_qa_baseurl', wt_QA_BASEURL);
	} else {
		$url = variable_get('wt_prod_baseurl', wt_PROD_BASEURL);
	}
  // test.webservices.illinois.edu
	$path = str_replace("/" . $drupal_base_path,"",$_SERVER['REQUEST_URI']);
   $url .= "/". $conf['path'] .  $path ; // .  $base_url_and_path;

  return $url; // check_url($url);

}

/**
 * Get web page as text
 *
 * @param $url
 *   request url to get content from
 *
 * @return
 *   webtools content as associative array
 */

function wt_api_get_web_page( $url ) {
	$options = array( 'http' => array(
		'user_agent'    => wt_api_USER_AGENT,        // who am i
		'max_redirects' => wt_api_MAX_REDIRECTS,              // stop after 10 redirects
		'timeout'       => variable_get('wt_api_request_timeout', wt_api_REQUEST_TIMEOUT),             // timeout on response
	) );
	$context = stream_context_create( $options );
	$page    = @file_get_contents( $url, false, $context );
	$result  = array( );
	if ( $page != false )
		$result['content'] = $page;
	else if ( !isset( $http_response_header ) )
		return null;    // Bad url, timeout

	// Save the header
	$result['header'] = $http_response_header;

	// Get the *last* HTTP status code
	$nLines = count( $http_response_header );
	for ( $i = $nLines-1; $i >= 0; $i-- )
	{
		$line = $http_response_header[$i];
		if ( strncasecmp( "HTTP", $line, 4 ) == 0 )
		{
			$response = explode( ' ', $line );
			$result['http_code'] = $response[1];
			break;
		}
	}

	return $result;
}

?>
