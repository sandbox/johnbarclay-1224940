

-- Webtools Modules Architecture  --

Integrations:
-- cck
-- feeds
-- exportables/features
-- views



Event Storage:
-- store all "event" content in cck content type with field
-- include all fields in webtools schema and allow for additional fields


Install Code:
-- Add or extend existing CCK type to include calendar fields
-- dependencies date


User Interface:
-- Use block interface for all exposed content


Themeing:
-- Work with Date Module + Calendar Module as feasible
-- Get HTML/XSLT from Lance if possible


Pushing Content:
-- Webtools
-- Exchange Web Services (EWS)
-- Feeds XML (Generic REST Webservice)


Sources:
-- Webtools XML via Feeds Module (custom or xpath fetcher and parser)



===========================================
WT_API module
===========================================
------
file: wt_api.module
wt_api_load() load function for wt_* configuration instance
wt_api_save() save function for wt_* configuration instance
wt_api_delete() delete function for wt_* configuration instance

wt_api_settings() load function for all settings
wt_api_create_feed_importer() function for creating importers

wt_api_blocks() generic block loader function for only block table data
wt_api_block_create() generic block create function
wt_api_generalized_block_info() block + wt_* data load function


------
wt_api table.
-- numeric_id: auto increment id
-- machine_name: machine name

-- wt_app_type = 'cal', 'blog', etc.
-- wt_id = 7,11, ..

-- drupal_structure:  block, importer, view
-- drupal_structure_id:  id of structure

-- enabled = TRUE | FALSE
-- path.  base path for views, etc
-- data. configuration data not falling into other fields


===========================================
wt_cal module
===========================================
Creates wt_event content type with all webtools schema fields
Provide feeds fetcher and parser for webtools calendar xml
Includes interface for creating importers for individual calendars

===========================================
wt_ewas module
===========================================
simply adds ewas javascript include to all pages when enabled.

===========================================
wt_blog.module
===========================================

===========================================
wt_skin.module (not needed and not implemented)
- convert skins and themes between drupal and webtools

===========================================
wt_dir.module (not needed and not implemented)
- import and export directory data between webtools and drupal
===========================================
