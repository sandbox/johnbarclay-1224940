<?php

function wt_adv_donation_index($donation_form_id = NULL) {

	$donation_forms = wt_adv_donation_form_data('wt_adv_donation_all');
	$items = array(l('Add Donation Form', WT_ADV_DONATION_ADMIN_BASE_PATH . '/add'));
	foreach ($donation_forms as $donation_form_id => $donation_form) {
		$view = ' ( '. l('view form', $donation_form['path']) . ' )';
		$items[] = l("Edit \"" . $donation_form['heading'] . "\"", WT_ADV_DONATION_ADMIN_BASE_PATH . "/edit/$donation_form_id") . $view;
	}
	return theme('item_list', array('items' => $items, 'title' => 'Donation Forms', 'type' => 'ul'));
}

function wt_adv_donation_form_id_from_args() {
	$arg = count(explode('/', WT_ADV_DONATION_ADMIN_BASE_PATH)) + 1;
	return  (arg($arg))? arg($arg) : arg($arg - 1);
}

function wt_adv_donation_form() {
	$donation_form_id =  wt_adv_donation_form_id_from_args();
	$donation_form = wt_adv_donation_form_data($donation_form_id);

	$form['path'] = array(
		'#type' => 'textfield',
		'#title' => t('Path.'),
		'#required' => 1,
		'#default_value' => $donation_form['path'],
		'#description' => 'Path where form should show.  In form such as "advancement/give" without the quotes.',
	);

	$form['heading'] = array(
		'#type' => 'textfield',
		'#title' => t('Form Heading.'),
		'#required' => 1,
		'#default_value' => @$donation_form['heading'],
		'#description' => 'Title to appear above form on web page.',
	);

	$form['prefix'] = array(
		'#type' => 'textarea',
		'#title' => t('Text Before Form'),
		'#required' => 0,
		'#default_value' => $donation_form['prefix'],
		'#description' => t('Title, form instructions, etc.'),
	);

	$form['suffix'] = array(
		'#type' => 'textarea',
		'#title' => t('Text After Form'),
		'#required' => 0,
		'#default_value' => $donation_form['suffix'],
		'#description' => t('Title, form instructions, etc.'),
	);

	$form['advancement'] = array(
		'#type' => 'fieldset',
		'#title' => 'Advancement Form Parameters',
		'#collapsible' => TRUE,
		'#collapsed' => ($donation_form['department'] && $donation_form['department_email'] && $donation_form['tracking_code'] && $donation_form['completed_url'] && $donation_form['change_data_url'])
		);

	$form['advancement']['department'] = array(
		'#type' => 'textfield',
		'#title' => t('Department.'),
		'#required' => 1,
		'#default_value' => $donation_form['department'],
		'#description' => 'e.g. education',
	);

	$form['advancement']['department_email'] = array(
		'#type' => 'textfield',
		'#title' => t('Department Email.'),
		'#required' => 1,
		'#default_value' => $donation_form['department_email'],
		'#description' => 'e.g. ed-alumni@illinois.edu',
	);

	$form['advancement']['tracking_code'] = array(
		'#type' => 'textfield',
		'#title' => t('Tracking Code.'),
		'#required' => 1,
		'#default_value' => $donation_form['tracking_code'],
		'#description' => 'e.g. 5W5JQ',
	);

	$form['advancement']['completed_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Completed URL.'),
		'#required' => 1,
		'#default_value' => $donation_form['completed_url'],
		'#description' => 'e.g. http://education.illinois.edu/advancement/gifts/thankyou',
	);

	$form['advancement']['change_data_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Change Data URL.'),
		'#required' => 1,
		'#default_value' => $donation_form['change_data_url'],
		'#description' => 'e.g. http://education.illinois.edu/advancement/gifts/makeagift',
	);



	foreach ($donation_form['funds'] as $fund_id => $fund_params) {
		if ($fund_id) {
			_wt_adv_donation_add_fieldset($form, $fund_id, $fund_params, TRUE);
		}
	}
	_wt_adv_donation_add_fieldset($form, NULL, array(), TRUE);
	_wt_adv_donation_add_fieldset($form, NULL, array(), TRUE);
	_wt_adv_donation_add_fieldset($form, NULL, array(), TRUE);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );

	if ($donation_form_id != 'add') {
		$form['delete'] = array(
			'#type' => 'submit',
			'#value' => 'Delete',
		);
	}
  $form['#validate'] = array('wt_adv_donation_form_validate');
  $form['#submit'] = array('wt_adv_donation_form_submit');

  return $form;
}

function wt_adv_donation_form_validate($form, &$form_state) {

	$form_state['storage']['id'] = wt_adv_donation_form_id_from_args();
	if ($form_state['clicked_button']['#value'] == 'Delete') {
		$donation_form_id = $form_state['storage']['id'];
		drupal_goto(WT_ADV_DONATION_ADMIN_BASE_PATH . "/delete/$donation_form_id");
	}
  $values = $form_state['values'];
	$old_form_id =  $form_state['storage']['id'];
	$new_form_id = str_replace('/', '__', $form_state['values']['path']);

	$menu_item = menu_get_item($form_state['values']['path']);
	if ($menu_item && $menu_item['page_callback'] != 'wt_adv_donation_page') {
		form_set_error('path', "Path ". $values['path'] . " is already used by in this site.");
	}

	$existing_form = wt_adv_donation_form_data($new_form_id);
	if ($old_form_id != $new_form_id && (boolean)$existing_form) {
		form_set_error('path', "Path ". $values['path'] . " is already used by another form");
	}

}

function wt_adv_donation_form_submit($form, &$form_state) {

	$donation_forms = wt_adv_donation_form_data('wt_adv_donation_all');
	$values = $form_state['values'];
	$old_form_id =  $form_state['storage']['id'];
	$form_id = str_replace('/','__', $values['path']);
	if ($form_state['storage']['id'] != 'add' && $form_state['storage']['id'] != $form_id) {
		unset($donation_forms[$form_state['storage']['id']]); // form id (path) changed, remove old form
	}
	$save_fields = wt_adv_donation_fields();
	if ($form_id) {
		$donation_form = array();
		foreach ($values as $key => $value) {
			$parts = explode('__', $key);
			if (count($parts) == 1) {
				if (in_array($key, $save_fields)) {
					$donation_form[$key] = $value;
				}
			}
			elseif (count($parts) == 2) {
				$field_name = $parts[0];
				$fund_id = $values['fund_id__' . $parts[1]];
				$donation_form['funds'][$fund_id][$field_name] = $value;
			}
		}

		$form_state = array(
			'redirect' => WT_ADV_DONATION_ADMIN_BASE_PATH,
		);
		$donation_form = wt_adv_donation_remove_empty_adds($donation_form);
		$donation_forms[$form_id] = $donation_form;
		wt_adv_donation_save($donation_forms);
		//@todo clear menu cache
		drupal_set_message('Advancement form Saved', 'status');
	}

}

function wt_adv_donation_settings_delete_confirm() {
  $arg = count(explode('/', WT_ADV_DONATION_ADMIN_BASE_PATH)) + 1;
	$donation_form_id = arg($arg);
	$donation_form = wt_adv_donation_form_data($donation_form_id);
	$form_name = $donation_form['heading'];
  $form['confirm'] = array(
    '#value' => t('Confirm Delete'),
  );
	$form['donation_form_id'] = array('#name' => 'donation_form_id','#type' => 'hidden', '#default_value' => $donation_form_id);
  return confirm_form(
    $form,
    t("Are you sure you want to delete \"{$form_name}\" form."),
    WT_ADV_DONATION_ADMIN_BASE_PATH,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

function wt_adv_donation_settings_delete_confirm_submit($form_id, &$form) {
	$donation_form_id = $form['values']['donation_form_id'];
  wt_adv_donation_delete($donation_form_id, TRUE);
	drupal_goto(WT_ADV_DONATION_ADMIN_BASE_PATH);
}

function wt_adv_donation_save($donation_forms) {

	/**
	$donation_forms = 'a:1:{s:29:"advancement__gifts__makeagift";a:11:{s:4:"path";s:27:"advancement/gifts/makeagift";s:7:"heading";s:51:"Make an Online Donation to the College of Education";s:6:"prefix";s:481:"Welcome! Please indicate the donation amount and which fund(s) you would like your donation directed to. If you are giving to more than one fund, please use your TAB key to move from one box to the next. After confirming the amount, press ENTER and you will be directed to the University of Illinois Foundation\'s secure Online Giving site for your personal and credit card information.

Thank you for your donation!

I would like my donation allocated to the following fund(s):";s:6:"suffix";s:0:"";s:5:"funds";a:15:{i:773584;a:4:{s:7:"fund_id";s:6:"773584";s:5:"title";s:40:"The Special Education Student Award Fund";s:11:"description";s:523:"Provides annual support for doctoral students using applied behavior analysis as their research framework to impact the domains of communication, problem behavior, or severe disabilities. The Fund was originally established to honor the careers of faculty members, Bob Henderson, Laura Jordan, and the late Laird Heal. Jim Halle\'s retirement in December 2010 and his long record of service and commitment to the field is being recognized through the addition of new donations to this Fund to which Jim\'s name will be added.";s:5:"order";s:2:"10";}i:33331425;a:4:{s:7:"fund_id";s:8:"33331425";s:5:"title";s:40:"College of Education Fund for Excellence";s:11:"description";s:100:"Gifts to this fund are used for student programs, faculty research, and enhancements in the College.";s:5:"order";s:2:"20";}i:33340291;a:4:{s:7:"fund_id";s:8:"33340291";s:5:"title";s:40:"William Chandler Bagley Scholarship Fund";s:11:"description";s:106:"Gifts to this fund provide merit scholarship for College of Education undergraduate and graduate students.";s:5:"order";s:2:"30";}i:340662;a:4:{s:7:"fund_id";s:6:"340662";s:5:"title";s:40:"The Richard C. Anderson Scholarship Fund";s:11:"description";s:282:"Will provide future support on an annual basis to a worthy graduate student with an interest in literacy. This fund will further speak to Dick\'s career as we acknowledge the important contributions that faculty can make as mentors to their graduate students and others in the field.";s:5:"order";s:2:"40";}i:33335149;a:4:{s:7:"fund_id";s:8:"33335149";s:5:"title";s:34:"College of Education Building Fund";s:11:"description";s:192:"Gifts to this fund will enable the College to create a physical home that allows for open collaboration and community cooperation that will define teaching and learning for this new generation";s:5:"order";s:2:"50";}i:33331514;a:4:{s:7:"fund_id";s:8:"33331514";s:5:"title";s:41:"The Minority Recruitment Enhancement Fund";s:11:"description";s:196:"Gifts to this fund provides financial support for our minority recruitment initiatives such as student visits from urban schools, guest speakers, special publications, and need-based scholarships.";s:5:"order";s:2:"60";}i:770343;a:4:{s:7:"fund_id";s:6:"770343";s:5:"title";s:36:"James D. Anderson Fund for Education";s:11:"description";s:293:"Gifts to his fund honor Professor Anderson and his dedication to the University of Illinois, and recognize an individual who has impacted the lives of students and faculty as he has sought to ensure the campus has steadily progressed toward the ideals of democracy, equity, and access for all.";s:5:"order";s:2:"70";}i:33334175;a:4:{s:7:"fund_id";s:8:"33334175";s:5:"title";s:19:"The Technology Fund";s:11:"description";s:191:"Gifts to this fund will be used to provide funding for instruction on incorporating technology into the classroom, to develop additional online courses, and to keep our computer labs current.";s:5:"order";s:2:"80";}i:33771647;a:4:{s:7:"fund_id";s:8:"33771647";s:5:"title";s:37:"The Community College Leadership Fund";s:11:"description";s:186:"Gifts to this fund support an annual award to recognize a full time working community college professional who is pursuing a doctorate degree in the Community College Leadership Program.";s:5:"order";s:2:"90";}i:33340168;a:4:{s:7:"fund_id";s:8:"33340168";s:5:"title";s:26:"AERA Mentorship Award Fund";s:11:"description";s:269:"Formerly known as the James Anderson Graduate Student Award, the AERA Mentorship Award is designed to assist Educational Policy Studies graduate students with travel expenses associated with attending the American Educational Research Association (AERA) annual meeting.";s:5:"order";s:3:"100";}i:33772610;a:4:{s:7:"fund_id";s:8:"33772610";s:5:"title";s:28:"The Ted Manolakes Award Fund";s:11:"description";s:118:"Gifts to this fund honor the career of Emeritus Professor Ted Manolakes and provide an annual merit based scholarship.";s:5:"order";s:3:"110";}i:33773358;a:4:{s:7:"fund_id";s:8:"33773358";s:5:"title";s:32:"The Frederick Rodgers Award Fund";s:11:"description";s:140:"Gifts to this fund honor Emeritus Professor Fred Rodgers and provide awards for doctoral students who will serve urban educational programs.";s:5:"order";s:3:"120";}i:33334099;a:4:{s:7:"fund_id";s:8:"33334099";s:5:"title";s:45:"Early Childhood Research and Practice Journal";s:11:"description";s:146:"(Formerly the Lilian Katz Fund). Gifts to this fund go toward supporting the Early Childhood Research and Practice Journal and related activities.";s:5:"order";s:3:"130";}i:33334247;a:4:{s:7:"fund_id";s:8:"33334247";s:5:"title";s:42:"University Primary School Enhancement Fund";s:11:"description";s:143:"Gifts to the fund go towards supporting and enhancing the operation of innovative programs in the College-affiliated University Primary School.";s:5:"order";s:3:"140";}i:33335117;a:4:{s:7:"fund_id";s:8:"33335117";s:5:"title";s:30:"Youth Literature Festival Fund";s:11:"description";s:212:"Gifts to this fund help to bring together families from throughout the area, those involved with the public sector, and a variety of community groups for one main goal: making literature the center of our lives. ";s:5:"order";s:3:"150";}}s:6:"submit";s:4:"Save";s:6:"delete";s:6:"Delete";s:13:"form_build_id";s:48:"form-IZbHzH0F9vjKEYSrNxIeNkPH438MIAvSKP_3ZQw3DkM";s:10:"form_token";s:43:"YIFy5hxZvyMiC8MMI6zT-4004x8YkNZdFSZPcAxBNSw";s:7:"form_id";s:20:"wt_adv_donation_form";s:2:"op";s:4:"Save";}}'

;
$donation_forms = unserialize($donation_forms);
**/

	variable_set('wt_adv_donation_forms', $donation_forms);
}

function wt_adv_donation_delete($donation_form_id, $message = FALSE) {
	$donation_forms = wt_adv_donation_form_data('wt_adv_donation_all');
	$donation_form = wt_adv_donation_form_data($donation_form_id);
	$form_title = $donation_form['title'];
	if (!isset($donation_forms[$donation_form_id])) {
		if ($message) {
			drupal_set_message(t("Could not find form $form_title with id of $donation_form_id."), 'warning', FALSE);
		}
	}
	else {
		if ($message) {
			drupal_set_message(t("Form \"{$form_title}\" Deleted."), 'status', FALSE);
		}
		unset($donation_forms[$donation_form_id]);
		wt_adv_donation_save($donation_forms);
	}

}




function _wt_adv_donation_add_fieldset(&$form, $fund_id, $fund_params, $collapsed = TRUE) {

	static $adds;
	if (!$fund_id) {
		$adds = ($adds) ? $adds + 1 : 1;
		$new_fund = TRUE;
		$fund_id = $adds;
		$fieldset_title = t("Add Fund");
	}
	else {
		$new_fund = FALSE;
		$fieldset_title = t("Edit Fund $fund_id, ". $fund_params['title']);
	}

	$fieldset_id = "fieldset__" . $fund_id;
	$form[$fieldset_id] = array(
		'#type' => 'fieldset',
		'#title' => $fieldset_title,
		'#collapsible' => TRUE,
		'#collapsed' => $collapsed,
		);

	$form[$fieldset_id]['fund_id__'. $fund_id] = array(
		'#type' => 'textfield',
		'#title' => t('Fund ID'),
		'#required' => 0,
		'#default_value' => ($fund_id < 100) ? '' : $fund_id,
		'#description' => 'Numeric fund id such as 340662',
	);

	$form[$fieldset_id]['title__'. $fund_id] = array(
		'#type' => 'textfield',
		'#title' => t('Fund Title'),
		'#required' => 0,
		'#default_value' => @$fund_params['title'],
		'#description' => t('Short title of fund'),
	);

	$form[$fieldset_id]['description__'. $fund_id] = array(
		'#type' => 'textarea',
		'#title' => t('Fund Description'),
		'#default_value' => @$fund_params['description'],
		'#cols' => 50,
		'#rows' => 5,
		'#required' => 0,
		'#description' => t('Fund Description'),
	);

	$form[$fieldset_id]['order__'. $fund_id] = array(
		'#type' => 'textfield',
		'#title' => t('Order'),
		'#required' => 0,
		'#default_value' => @$fund_params['order'],
		'#description' => 'Order to be displayed on form.  Lowest numbers appear first.',
	);
}

function wt_adv_donation_remove_empty_adds($donation_form) {
  function no_fund_id($var) {
		return (boolean)($var['fund_id']);
	}
	$donation_form['funds'] = array_filter($donation_form['funds'], 'no_fund_id');
  return $donation_form;

}


