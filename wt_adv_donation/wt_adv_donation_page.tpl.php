<?php

/**
 * available variables
 *
 * $donation_form_id
 * $path
 * $heading
 * $prefix
 * $suffix
 * $department
 * $department_email
 * $completed_url
 * $change_data_url
 * $tracking_code
 *
 * $funds[<fund_id>] => array(
 *   'fund_id' => ,
 *   'title' => ,
 *   'description' => ,
 *   'order' = >
 * )
 *
 *
 */
?>

<?php print $prefix; ?>
<?php wt_adv_donation_page_js($funds) ?>

<form action="https://ps.uif.uillinois.edu/Gifts/Payment.aspx" method="post" class="wt_adv_donation_form" id="wt_adv_donation_form_<?php print $department; ?>" name="FrontPage_Form1" onsubmit="return My_Form_Validator(this)">

<input name="DEPARTMENT" type="hidden" value="<?php print $department; ?>"/>
<input name="DEPARTMENT_EMAIL" type="hidden" value="<?php print $department_email; ?>"/>
<input name="TRACKING_CODE" type="hidden" value="<?php print $tracking_code; ?>"/>
<input name="ALLOW_EDITS" type="hidden" value="N"/>
<input name="COMPLETED_URL" type="hidden" value="<?php print $completed_url; ?>"/>
<input name="CHANGE_DATA_URL" type="hidden" value="<?php print $change_data_url; ?>"/>

<table cellpadding="5">
<tbody>

<?php

// sort on order
foreach ($funds as $fund_id => $fund) {
  if ($fund_id) {
    $sort[$fund_id] = $fund['order'];
  }
}
asort($sort);

// list all funds
$i = 0;
foreach ($sort as $fund_id => $order) {
  $fund = $funds[$fund_id];
  $i++;
  $fund_title = $fund['title'];
  $fund_description = $fund['description'];
  ?>
<tr class="fund-item">
<td class="input-col"><span class="dollar-sign">$</span><input class="fund-amount" language="javascript" name="GIFT_AMOUNT<?php print $i;?>" onchange="amountchange();" size="8" value="0.00"/></td>
<td class="description-col">
  <input name="FUND<?php print $i;?>" type="hidden" value="<?php print $fund_id; ?>/<?php print $fund_title; ?>"/>
  <?php print $fund_title; ?>
  <?php print $fund_description; ?>
</td>
</tr>


<?php
}
?>



<tr class="fund-item">
  <td class="input-col">
    <span class="dollar-sign">$</span><input class="fund-amount" language="javascript" name="totalamount" onchange="return amountchange()" size="8" value="0.00"/>
  </td>
  <td>
    <b>Total</b>
  </td>
</tr>

</tbody>
</table>
<center><input name="B1" type="submit" value="Continue Donation"/></center>
</form>

<?php print $suffix; ?>


<?php
function wt_adv_donation_page_js($funds) {
?>

<script language="javascript">

// ---------------------------------------------------
// My_Form_Validator()- Client side form validator
// ---------------------------------------------------
function My_Form_Validator(form){

	if (form.totalamount.value == 0){
		alert("Please enter a donation to at least one fund before submitting.");
		return false;
	}

} // End of My_Form_Validator


// -----------------------------------------------------------------
// CalcDisplayAmount - convert numeric entry to formatted text value
// -----------------------------------------------------------------
function CalcDisplayAmount(amt)
{
	var displayAmt;
	var amtValue;
	if (isNaN(amt)== true){   //not a valid number
		displayAmt = "0.00";
		return displayAmt;
	}

  if (amt.length==0) {		// no value at all
		displayAmt = "0.00";
		return displayAmt;
	}

// round to 2 places after the decimal point
	amtValue = parseFloat(amt);
	amtValue = Math.round(amtValue * 100) / 100;

	if (amtValue < 0) amtValue = 0;

	if (amtValue == Math.round(amtValue)) {
		displayAmt = amtValue + ".00";
	}
	else if (amtValue * 10 == Math.round(amtValue * 10)) {
		displayAmt = amtValue + "0";
	}
	else {
		displayAmt = amtValue;
	}

	return displayAmt;
}
// -----------------------------------------------------------------
// amountchange() - called when any amount changes
// -----------------------------------------------------------------
function amountchange()    {

	var total;

	/* change display format */

	total = 0;
<?php
$i = 0;
foreach ($funds as $fund_id => $fund) {
  $i++;
?>

document.FrontPage_Form1.GIFT_AMOUNT<?php print $i; ?>.value=CalcDisplayAmount(document.FrontPage_Form1.GIFT_AMOUNT<?php print $i; ?>.value);
total = total + parseFloat(document.FrontPage_Form1.GIFT_AMOUNT<?php print $i; ?>.value);

<?php
 }
?>

total = Math.round(total * 100) / 100;
document.FrontPage_Form1.totalamount.value=CalcDisplayAmount(total);

}

 </script>

<?php
}
?>
